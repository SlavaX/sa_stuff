**G**: 3118G  
**В-К**: Вираж-Контроль  
**О-К**: Омск-Круг  
**О-С**: Омск-Старт

>**G**: G, взлёт маршрут 450 по 1013  
>**В-К**: _G, взлёт набор 450 по 1013 Новинка 5 минута_  
>**G**: взлёт, G   

>**G**: G, в наборе 450 по 1013, Новинка 10 минута  
>**В-К**: _набирайте 450 по 1013 работайте с О-К 119.0_  
>**G**: 450 по 1013 на связи с О-К 119.0 до обратного, G  

**119.0 Mhz Омск-Круг**

>**G**: О-К, G Добрый день  
>**О-К**: _G, О-К, опознаны_  
>**G**: G, взлёт Калачёва 5 минута 450 по 1013 вход в Вашу зону Новинка 10 минута  
>**О-К**: _G, сохраняйте 450 давление 1013 ГПа, вход в зону разрешаю, Новинку доложить_   
>**G**: 1013 установлено 450 сохраняю, вход разрешили, Новинку доложу, G

**Новинка - Мельничное**

>**G**: G, Новинка 450 по 1013 Мельничное 20 минута  
>**О-К**: _G, сохраняйте 450 по 1013, Мельничное доложить_  
>**G**: 450 по 1013 Мельничное доложу, G

**прошли Лузино слушаем             ATIS / 126.4 Mhz/**

**Мельничное - Северная зона**

>**G**: G, Мельничное 450 по 1013, Северная зона 25 минута  
>**О-К**:  _G, сохраняйте 450 по 1013, Северную зону доложить_  
>**G**: 450 по 1013 Северную зону доложу, G

**Северная зона**

>**G**: G, Северная зона 450 по 1013, заход ПВП, информация Oskar  
>**О-К**: _G, заход оп схеме ПВП на полосу 25 разрешаю, подход к 4-му доложить_    
>**G**: заход разрешили, полоса 25, подход к 4-му доложу, G   

(Если поставили в ожидание) Вираж в Северной зоне до разрешения, G

**Подход к 4-му**

>**G**: G, подход к 4-му  
>**О-К**: _G, работайте с О-С на текущей частоте_  
>**G**: работаю с О-С на текущей частоте, G   

>**G**: G, О-С, добрый день, на 4-м, полосу наблюдаю, к посадке готов  
>**О-С**: _G, О-С, день добрый, ветер у земли 310 4 метра, ВПП25, посадку разрешаю_   
>**G**: условия принял, посадку разрешили, G   

**Посадка**

>**G**: G, посадка  
>**О-С**: _G, посадка 30 минута_   
>**G**: информацию принял, готовность к взлёту доложу, G  

**Перед взлётом**

>**G**: G, к взлёту готов  
>**О-С**: _G, (условия) взлёт разрешаю_   
>**G**: условия принял, взлёт разрешили, взлетаю, G  

**Взлёт набор 150 м в Южную зону**

>**G**: О-К, G, взлёт 31 минута, левым на безопасной, Южная Зона 38 минута. Разрешите 450 по 1013  
>**О-К**: _G, не ниже безопасной, давление 1013 ГПа, Южную зону доложить_   
>**G**: набор 450 по 1011, Южную Зону доложу, G  

**Южная зона - Мирная долина**

>**G**: G, Южная Зона 450 по 1013 Мирная долина 45 минута   
>**О-К**: _G, сохраняйте 450 по 1013, Мирную долину доложить_   
>**G**: 450 по 1011, Мирную долину доложу, G  

**Мирная долина - Новинка**

>**G**: G, Мирная долина 450 по 1013, Новинка 50 минута. Разрешите кратко отработать с В-К на 123.3.   
>**О-К**: _G, сохраняйте 450 по 1013, Новинку доложить, с В-К на 123.3 разрешаю_   
>**G**: 450 по 1011, Новинку доложу, кратко с В-К разрешили, G  

**123.3 Mhz Вираж-Контроль**

>**G**: В-К, G  
>**В-К**: _G, В-К_  
>**G**: G, прошёл Мирную долину 450 по 1013, вход в зону Новинка 50 минута  
>**В-К**: _G, сохраняйте 450 по 1013, Новинку доложить_  
>**G**: G, 450 по 1013, Новинку доложу  

**119.0 Mhz Омск-Круг**

>**G**: G, на связи  
>**О-К**: _О-К, G, на связи_  
>**G**: G, Новинка 450 по 1013, связь по направлению установил  
>**О-К**: _G, работайте с В-К 123.3_  
>**G**: работаю с В-К на 123.3, G  

**123.3 Mhz Вираж-Контроль**

**Новинка - Точка**

>**G**: G, Новинка 450 по 1013, точка 57 минута  
>**В-К**: _G, сохраняйте 450 по 1013, Точку доложить_  
>**G**:	450 по 1013, точку доложу, G

**Точка - 3-ий**

>**G**: G, Точка, 450 по 1013, к 3-му 200 за бортом 2088G  
>**В-К**: _G, к 3-му 200_  
>**G**: к 3-му 200, G  


![](m2_unol_unoo_unol.jpeg)
